//To create icon
import L from 'leaflet';

import mapMarkerImg from '../images/map-marker.svg';

const mapIcon = L.icon({
    iconUrl: mapMarkerImg,  
    iconSize: [50, 60],
    //definição de qual a posição do icon que reperesenta o ponto (por defeito é no centro da imagem)
   //mas no nosso icon queremos a ponta (que fica no centro do eixo x, e no ponto mais baixo no eixo y)
    iconAnchor: [25, 60],
    //posição do popup
    popupAnchor: [0, -60]
  });

export default mapIcon;

import React, { useEffect, useState  } from 'react'
import {Link} from 'react-router-dom'
import {FiPlus, FiArrowRight} from 'react-icons/fi'
import {Map, TileLayer, Marker, Popup} from 'react-leaflet'

//the image to use in marker
import mapMarkerImg  from '../images/map-marker.svg'

import '../styles/pages/orphanages-map.css'
import mapIcon from '../utils/mapIcon'
import api from '../services/api'

// const mapIcon = Leaflet.icon({
//     iconUrl:mapMarkerImg,
//     iconSize:[25,55],
//     //definição de qual a posição do icon que reperesenta o ponto (por defeito é no centro da imagem)
//     //mas no nosso icon queremos a ponta (que fica no centro do eixo x, e no ponto mais baixo no eixo y)
//     iconAnchor:[15,55],
//     //posição do popup
//     popupAnchor:[170,2]
// })
interface orphanage {
    id:number,
    name: string,
    latitude: number,
    longitude: number,
    about: string,   
}

function OrphanagesMap() {
    const [orphanages, setOrphanages] = useState<orphanage[]>([]);
    console.log( orphanages );
    useEffect(() => {
        api.get('orphanages').then(response => {
            setOrphanages(response.data);
        });
    }, []);


    return (
        <div id="page-map">
             <aside>
                <header>
                    <img src= {mapMarkerImg} alt="Happy" />

                    <h2>Escolha um orfanato no mapa</h2>
                    <p>Muitas crianças estão à sua espera da sua visita :) </p>
                </header>
                
                <footer>
                    <strong>Portugal</strong>
                    <span>Lisboa</span>
                </footer>
            </aside> 
            <Map 
                center={[38.7789399,-9.1882612]} 
                zoom={15} 
                style={{width:'100%', height:'100%'}}
                >
                {/* <TileLayer url="https://a.tile.openstreetmap.org/{z}/{x}/{y}.png" /> */}
                <TileLayer 
                    url={`https://api.mapbox.com/styles/v1/mapbox/light-v10/tiles/256/{z}/{x}/{y}@2x?access_token=${process.env.REACT_APP_MAPBOX_TOKEN}`} 
                />

                {orphanages.map(orph => {
                    return(
                        <Marker key= {orph.id} position= {[orph.latitude,orph.longitude]} icon= {mapIcon}>
                        <Popup closeButton={false} minWidth= {240} maxWidth= {240} className= "map-popup">
                            {orph.name}
                            <Link to={`/orphanages/${orph.id}`}>
                            <FiArrowRight size={20} color="#FFF"/>
                            </Link>
                        </Popup>
                    </Marker>
                    )
                })}

            </Map>
            
            <Link to="/orphanages/create" className="create-orphanage" >
                <FiPlus size={32} color="#FFF"/>
            </Link>  
        </div>
    );
}

export default OrphanagesMap;
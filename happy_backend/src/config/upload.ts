import multer from 'multer';
//path pertence ao node e é uma forma de fazer caminhos relativos
import path from 'path';

export default {
    storage: multer.diskStorage(
        {
            //__dirname : da-me o caminho todo até à diretoria que estou atualmente (até à pasta config)
            //a seguir separado por virgulas, vou fazer o caminho até à pasta onde vao estar os uploads
            // não se coloca o '/' porque difere consoante o SO seja linux, mac, windowns
            destination: path.join(__dirname,'..','..', 'uploads'),
            filename: (request, file, cb) => {
                //file.originalname dá-me o nome do ficheiro original e a extensão dele
                const filename = `${Date.now()}-${file.originalname}` 
                //o 1ºparametro é um erro, como a probabilidade de erro é pouca, passamos null
                //o2º parametro é o nome
                cb(null,filename);
            }
        }
    )
    
}
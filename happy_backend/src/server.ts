import express from 'express';
import 'express-async-errors';
import './database/connection';
import path from 'path';
import cors from 'cors';

import routes from './routes'; 
import errorHandler from './errors/handle';


const app = express();

//Registo de middlewares

//cors permite que outras aplicaçoes de FE, em outros dominios, comuniquem com esta api
//pode levar ainda um obejcto com propriedade origin para configurar a aplicação de FE que quero dar permissao (que é a forma correta)
app.use(cors());

app.use(express.json()); //registar um middleware que permite receber pedidos em formato JSON
//deve vir depois de express.json
app.use(routes);
//para aceder ao url das imagens, basta configurar a rota '/uploads' para ir até à pasta uploads
app.use('/uploads', express.static(path.join(__dirname,'..','uploads')));
app.use(errorHandler);

app.listen(3333);



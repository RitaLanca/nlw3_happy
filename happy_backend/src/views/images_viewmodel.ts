import path from 'path'
import Image from "../models/Image";

 
 export default {
     render(image: Image) {
         return {
             id: image.id,
             //o ideal será usar variáveis de ambiente (VER https://blog.rocketseat.com.br/variaveis-ambiente-nodejs/)
             url:`http://localhost:3333/uploads/${image.path}`
         }
     },
     renderMany(images: Image[]) {
        return images.map(img=> this.render(img));
    }
 }
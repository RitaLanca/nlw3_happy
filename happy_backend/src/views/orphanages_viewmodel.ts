import ImageViewModel from '../views/images_viewmodel'
import Orphanage from '../models/Orphanage'

export default {
    render(orphanage: Orphanage) {
        return {
            id: orphanage.id,
            //trocar name por nome para testar error handling
            name: orphanage.name,
            latitude: orphanage.latitude,
            longitude: orphanage.longitude,
            about: orphanage.about,
            instructions: orphanage.instructions,
            opening_hours: orphanage.opening_hours,
            open_on_weekends: orphanage.open_on_weekends,
            images: ImageViewModel.renderMany(orphanage.images)
        }
    },
    renderMany(orphanages: Orphanage[]) {
        return orphanages.map(orph=> this.render(orph));
    }
}
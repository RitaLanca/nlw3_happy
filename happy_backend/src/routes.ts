import {Router} from 'express'
import multer from 'multer'

import uploadConfig from '../src/config/upload'
import OrphanagesController from './controllers/OrphanagesController';

const routes = Router();
//estamos a passar as configurações do upload para o multer
const upload = multer(uploadConfig);

routes.get('/orphanages',OrphanagesController.index);
routes.get('/orphanages/:id',OrphanagesController.show);
//trocar app por Routes
routes.post('/orphanages',upload.array('images'), OrphanagesController.create);


export default routes;

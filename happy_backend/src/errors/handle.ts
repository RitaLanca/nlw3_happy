import {ErrorRequestHandler} from 'express'
//importar o ValidationError do Yup
import {ValidationError} from 'yup'

interface ValidationErrors {
    //o formato deste objeto é uma key do tipo string e o value é um array de msg de erros
    // {
    //   "name": ['obrigatório','minimo de caracteres'],
    //   "latitude": ['obrigatório','minimo de caracteres']
    // }
    [key:string]:string[]
}

const errorHandler: ErrorRequestHandler = (error, request, response, next) => {
    if(error instanceof ValidationError)
    {
        //quero retornar os erros para o frontend de forma diferente
        let errors:ValidationErrors = {}
        error.inner.forEach(err =>{
            errors[err.path] = err.errors;
        });
        return response.status(400).json({message:'Validation fails', errors});
    }

    console.error(error);
    return response.status(500).json({message:'Internal Server Error'});
};

export default errorHandler;
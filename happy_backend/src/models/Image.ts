import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm'

import Orphanage from './Orphanage';

@Entity('Images')
export default class Image {
    @PrimaryGeneratedColumn('increment')
    id: number;
    @Column()
    path: string;
   
    //nao se coloca orphanage_id: esta relação vai ser feita no Orphanage.ts

    //para a relação inversa
    @ManyToOne(() =>Orphanage,orphanage => orphanage.images)
    @JoinColumn({name:'orphanage_id'})
    orphanage: Orphanage;
}
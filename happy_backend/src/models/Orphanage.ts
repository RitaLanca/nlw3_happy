import {Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn} from 'typeorm'

import Image from './Image';

@Entity('Orphanages')
export default class Orphanage {
    @PrimaryGeneratedColumn('increment')
    id: number;
    @Column()
    name: string;
    @Column()
    latitude: number;
    @Column()
    longitude: number;
    @Column()
    about: string;
    @Column()
    instructions:string;
    @Column()
    opening_hours: string;
    @Column()
    open_on_weekends: string;

    //Dentro do One To Many passo como options o cascade para o insert e o update
    //Assim posso passar logo na rota o array "images" preenchido com as imagens, que estas são logo guardadas na BD 
    @OneToMany(()=> Image, image => image.orphanage, {
        cascade: ['insert','update']
    })
    @JoinColumn({name:'orphanage_id'})
    images: Image[]
}
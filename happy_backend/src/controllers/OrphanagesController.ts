import {Request, Response} from 'express'
import {getRepository} from 'typeorm'
import * as Yup from 'yup'

import OrphanageViewModel from '../views/orphanages_viewmodel'
import Orphanage from '../models/Orphanage';


export default {

    async index(request:Request, response:Response) {
        const orphanagesRepository = getRepository(Orphanage);
        
        const orphanages = await orphanagesRepository.find({
            //para incluir as imagens do objeto orphanage
            relations:['images']
        });
        
        return response.json(OrphanageViewModel.renderMany(orphanages));
    },
    async show(request:Request, response:Response) {
        const {id} = request.params;

        const orphanagesRepository = getRepository(Orphanage);

        const orphanage = await orphanagesRepository.findOneOrFail(id,
            {
                relations: ['images']
            });

        return response.json(OrphanageViewModel.render(orphanage));
    },
    async create(request: Request, response:Response) {
        

        const {
             //TODO:trocar name por nome para testar error handling
            name, 
            latitude, 
            longitude,
            about, 
            instructions, 
            opening_hours, 
            open_on_weekends
        } = request.body;

        const orphanagesRepository = getRepository(Orphanage);

        //as imagens não vêm no request body mas sim no request files
        //temos que colocar "as Express.Multer.File[]" para afirmar o tipo 
        const requestImages = request.files as Express.Multer.File[];
        const images = requestImages.map(img => {
            return { path: img.filename}
        });

        //preparar validação de dados
        const data = {
            name, 
            latitude, 
            longitude,
            about, 
            instructions, 
            opening_hours, 
            open_on_weekends: open_on_weekends === 'true',
            images
        }
    
        const schema = Yup.object().shape({
            name: Yup.string().required("Nome é obrigatório"),
            latitude: Yup.number().required(),
            longitude: Yup.number().required(),
            about: Yup.string().required().max(300),
            instructions: Yup.string().required(),
            opening_hours: Yup.string().required(),
            open_on_weekends: Yup.boolean().required(),
            images: Yup.array(
                Yup.object().shape({
                    path: Yup.string().required()
                })
            )
        });

        await schema.validate(data, {
            //false porque queremos que seja retornado todos os campos que estão invalidos.
            //Se for true, mal ache um campo inválido, aborta a validação e só devolve esse erro
            abortEarly: false            
        });
        //só cria o orphanage, mas não salva na DB
        const orphanage = orphanagesRepository.create(data);
    
        await orphanagesRepository.save(orphanage);
    return response.status(201).json(orphanage);
    }


}